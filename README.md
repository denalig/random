* random-gc.py uses remote servers garbage collection stats as source of entropy.
* Source of entropy should not be available to to others because the accessibility of the data negates the randomness. 
* The reason I picked remote server stats is that it is readily available source of fairly random data and for demonstration purposes only.
  In real life, the stats might come from locally run applications.


### Prerequisites:

* Python3
* must run with root permissions to allow write access to /dev/random file



### Example Use

```
[root@localhost random]# ./random-gc.py -h
usage: random-gc.py [-h] [--entropy]

optional arguments:
  -h, --help  show this help message and exit
  --entropy   add to entropy pool without generating random numbers
  
```
  
  
  Running with --entropy flag allows demonstration of entropy building
  
```
[root@localhost random]# ./random-gc.py --entropy 

Generating entropy only!


after adding to entropy pool 3822

after adding to entropy pool 3835
```
  
  
  
 Running in default mode  generates random bytes 
 
```
 [root@localhost random]# ./random-gc.py 
b'jgxL"\x1d\x86\x9a:N'b'\xb1F\xeagFG\xefu\xc5.'b'c\xe0\\\x8c\x94o\x84\x0eE\xd7'b'\xe8\x9bg\x1a\x88\x0ca\xbb\n\xbe'b'l\nh\x9d\x1b\x9cm}\x93K'b'\x13\x1e\x93\x9c\xd2Q\xba+\x15O'b'\x93\xee\xacLZ\xc4`\x94\x7f\x8b'b']\xca\xa2\x90M\xbc\x08\xbeC^'b'(\xe6\xbe\xf2\x03\x05{.\xa8E'b'\x8c\x04L\xaa!C\r\x8d\x96\x1e'xb's\xa1]\x02`+\x83c
 
```
  
  
  