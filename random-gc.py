#!/usr/bin/env python3


import os
import threading
from time import sleep
import urllib.request
import json
import struct
import fcntl
import hashlib
import sys

RNDADDENTROPY = 1074287107


def gen_random(entropy_only):
    try:
        shutdown_event = threading.Event()
        threads = []
        t1 = threading.Thread(target=print_random, args=(shutdown_event,))
        threads.append(t1)
        if not entropy_only:
            t1.start()
        t2 = threading.Thread(target=add_entropy, args=(int(entropy_only),shutdown_event,))
        threads.append(t2)
        t2.start()

        while t1.is_alive():
            t1.join(timeout=1.0)
        while t2.is_alive():
            t2.join(timeout=1.0)

    except (KeyboardInterrupt, SystemExit):
        shutdown_event.set()
        print()


def print_random(shutdown_event):
    while not shutdown_event.is_set():
        print(os.urandom(10), end='', flush=True)
        sleep(0.2)


def add_entropy(entropy_only, shutdown_event):
    hash = hashlib.sha512()
    while not shutdown_event.is_set():
        curr_stats = get_gc_stats()
        update_entropy(curr_stats, hash, entropy_only)
        if entropy_only:
            print("after adding to entropy pool %s" % os.popen('cat /proc/sys/kernel/random/entropy_avail').read())

        sleep(5)


def get_gc_stats():
    urls = ["http://prometheuslb-v3r2qa-2051358502.us-west-2.elb.amazonaws.com/api/v1/query?query=erlang_vm_statistics_garbage_collection_bytes_reclaimed",
            "http://prometheuslb-v3r2qa-2051358502.us-west-2.elb.amazonaws.com/api/v1/query?query=erlang_vm_statistics_garbage_collection_number_of_gcs",
            "http://prometheuslb-v3r2qa-2051358502.us-west-2.elb.amazonaws.com/api/v1/query?query=erlang_vm_statistics_garbage_collection_words_reclaimed",
            "http://prometheuslb-v3r2qa-2051358502.us-west-2.elb.amazonaws.com/api/v1/query?query=erlang_vm_statistics_reductions_total",
            "http://prometheuslb-v3r2qa-2051358502.us-west-2.elb.amazonaws.com/api/v1/query?query=erlang_vm_statistics_runtime_milliseconds"]

    l = []

    for url in urls:
        req = urllib.request.Request(url)
        r = urllib.request.urlopen(req).read()
        cont = json.loads(r.decode('utf-8'))
        for item in cont['data']['result']:
            val = str(item['value'][1]) + str(item['value'][0])
            l.append(val)

    return l


def update_entropy(stats, hash, entropy_only):
    random_fd = os.open('/dev/random', os.O_WRONLY)

    try:
        for i in range(len(stats)):
            rawdata = str(stats[i]).encode('utf-8')
            hash.update(rawdata)
            data = hash.digest()
            size = len(data)
            format = 'ii%is' % size
            entropy_data = struct.pack(format, 8 * size, size, data)
            #if entropy_only:
             #   print("added %i bytes entropy " % size)

            fcntl.ioctl(random_fd, RNDADDENTROPY, entropy_data)

    finally:
        os.close(random_fd)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--entropy", help="add to entropy pool without generating random numbers",
                        action="store_true")
    args = parser.parse_args()
    if args.entropy:
        print("\n\nGenerating entropy only!\n\n")

    gen_random(args.entropy)
